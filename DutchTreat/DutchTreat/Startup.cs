﻿using AutoMapper;
using DutchTreat.Data;
using DutchTreat.Data.Contracts;
using DutchTreat.Data.Entities;
using DutchTreat.Helpers;
using DutchTreat.Services.MailService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace DutchTreat
{
    public class Startup
    {
        #region Constants.

        protected const string DutchConnectionStringName = "DutchConnectionString";
        protected const string ShouldDisableSSL = "DisableSSL";

        #endregion

        protected IConfiguration _config { get; }
        public IHostingEnvironment _environment { get; }

        public Startup(IConfiguration config, IHostingEnvironment environment)
        {
            _config = config;
            _environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            if (_environment.IsDevelopment())
            {
                Mapper.Reset();
            }

            services.AddIdentity<User, IdentityRole>(cfg =>
            {
                cfg.User.RequireUniqueEmail = true;
            })
            .AddEntityFrameworkStores<DutchContext>();
            services.AddAuthentication()
                    .AddJwtBearer(options =>
                    {
                        options.ForwardDefaultSelector = AuthenticationHelper.ForwardDefaultSelector;
                        options.TokenValidationParameters = new TokenValidationParameters()
                        {
                            ValidIssuer = _config[AuthenticationHelper.TokenIssuerConfigName],
                            ValidAudience = _config[AuthenticationHelper.TokenAudienceConfigName],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config[AuthenticationHelper.SymmetricSecurityKeyConfigName]))
                        };
                    });

            services.AddDbContext<DutchContext>(cfg=>
            {
                cfg.UseSqlServer(_config.GetConnectionString(DutchConnectionStringName));
            });

            services.AddAutoMapper();

            services.AddTransient<IMailService, NullMailService>();
            services.AddTransient<DutchSeeder>();
            services.AddScoped<IDutchRepository, DutchRepository>();
            services.AddMvc(options=>
            {
                if((_environment.IsProduction()) && (!Convert.ToBoolean(_config[ShouldDisableSSL])))
                {
                    options.Filters.Add(new RequireHttpsAttribute());
                }
            })
                .AddJsonOptions(jsonOptions => jsonOptions.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseFileServer(new FileServerOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(env.ContentRootPath, "node_modules")
                ),
                    RequestPath = "/node_modules",
                    EnableDirectoryBrowsing = true
                });
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(cfg=>
            {
                cfg.MapRoute("Default",
                    "{controller}/{action}/{id?}",
                    new { controller = "App", Action = "Index" });
            });

            if(env.IsDevelopment())
            {
                using (var scope = app.ApplicationServices.CreateScope())
                {
                    var seeder = scope.ServiceProvider.GetService<DutchSeeder>();
                    seeder.Seed().Wait();
                }
            }
        }
    }
}