﻿using DutchTreat.Data.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Data
{
    public class DutchSeeder
    {
        protected DutchContext _context { get; }
        IHostingEnvironment _hosting { get; }
        public UserManager<User> _userManager { get; }

        public DutchSeeder(DutchContext context,
            IHostingEnvironment hosting,
            UserManager<User> userManager)
        {
            _context = context;
            _hosting = hosting;
            _userManager = userManager;
        }

        public async Task Seed()
        {
            _context.Database.Migrate();

            var user = await _userManager.FindByEmailAsync("test@test.com");

            if (user == null)
            {
                user = new User("TestUser'sFirstName", "TestUser'sLastName", "test@test.com", "test@test.com");

                var userCreationResult = await _userManager.CreateAsync(user, "P@ssw0rd!");
                if (userCreationResult != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Failed to create default user");
                }
            }

            if (!_context.Products.Any())
            {
                var filepath = Path.Combine(_hosting.ContentRootPath, "Data/art.json");
                var json = File.ReadAllText(filepath);
                var products = JsonConvert.DeserializeObject<IEnumerable<Product>>(json);
                _context.Products.AddRange(products);

                var orderItems = new List<OrderItem>()
                {
                    new OrderItem(products.OrderBy(p => p.Id).Last(), 5, products.OrderBy(p => p.Id).Last().Price)
                };
                var order = new Order(DateTime.Now, "12345", orderItems, user);

                _context.Orders.Add(order);

                _context.SaveChanges();
            }
        }
    }
}