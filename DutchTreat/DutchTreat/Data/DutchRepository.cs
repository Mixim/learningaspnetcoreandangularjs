﻿using DutchTreat.Data.Contracts;
using DutchTreat.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DutchTreat.Data
{
    public class DutchRepository : IDutchRepository
    {
        protected DutchContext _context { get; }
        protected ILogger<DutchRepository> _logger { get; }

        public DutchRepository(DutchContext context, ILogger<DutchRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<Product> GetAllProducts()
        {
            IEnumerable<Product> returnValue;
            try
            {
                _logger.LogInformation("GetAllProducts was called.");

                returnValue = _context.Products
                    .OrderBy(p => p.Title)
                    .ThenBy(p => p.Id)
                    .ToList();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Failed to get all products: \"{ex}\".");
                returnValue = Enumerable.Empty<Product>();
            }
            return returnValue;
        }

        public IEnumerable<Product> GetProductsByCategory(string category)
        {
            IEnumerable<Product> returnValue;
            try
            {
                _logger.LogInformation("GetProductsByCategory was called.");

                returnValue = _context.Products
                           .Where(p => p.Category == category)
                           .OrderBy(p => p.Title)
                           .ThenBy(p => p.Id)
                           .ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get products by category: \"{ex}\".");
                returnValue = Enumerable.Empty<Product>();
            }
            return returnValue;
        }

        public bool SaveAll()
        {
            bool returnValue;
            try
            {
                _logger.LogInformation("SaveAll was called.");

                returnValue = _context.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to save all products: \"{ex}\".");
                returnValue = false;
            }
            return returnValue;
        }

        public IEnumerable<Order> GetAllOrders(bool includeItems)
        {
            IEnumerable<Order> returnValue;
            try
            {
                _logger.LogInformation("GetAllOrders was called.");

                if (includeItems)
                {
                    returnValue = _context.Orders
                        .Include(p => p.Items)
                        .ThenInclude(item => item.Product)
                        .OrderBy(p => p.Date)
                        .ThenBy(p => p.Id)
                        .ToList();
                }
                else
                {
                    returnValue = _context.Orders
                        .OrderBy(p => p.Date)
                        .ThenBy(p => p.Id)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get all orders: \"{ex}\".");
                returnValue = Enumerable.Empty<Order>();
            }
            return returnValue;
        }

        public IEnumerable<Order> GetAllOrdersByUser(string username, bool includeItems)
        {
            IEnumerable<Order> returnValue;
            try
            {
                _logger.LogInformation("GetAllOrdersByUser was called.");

                if (includeItems)
                {
                    returnValue = _context.Orders
                        .Where(o => o.User.UserName == username)
                        .Include(o => o.Items)
                        .ThenInclude(item => item.Product)
                        .OrderBy(o => o.Date)
                        .ThenBy(o => o.Id)
                        .ToList();
                }
                else
                {
                    returnValue = _context.Orders
                        .Where(o => o.User.UserName == username)
                        .OrderBy(o => o.Date)
                        .ThenBy(o => o.Id)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get all orders by user: \"{ex}\".");
                returnValue = Enumerable.Empty<Order>();
            }
            return returnValue;
        }

        public Order GetOrderById(string username, int id)
        {
            Order returnValue;
            try
            {
                _logger.LogInformation("GetOrderById was called.");

                returnValue = _context.Orders
                    .Include(o => o.Items)
                    .ThenInclude(item => item.Product)
                    .SingleOrDefault(o => (o.Id == id) && (o.User.UserName == username));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get order by id: \"{ex}\".");
                returnValue = null;
            }
            return returnValue;
        }

        public void AddEntity<T>(T entity) where T : class
        {
            try
            {
                _logger.LogInformation("AddEntity was called.");

                _context.Add(entity);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to add entity: \"{ex}\".");
            }
        }

        public void AddOrder(Order newOrder)
        {
            foreach(var curItem in newOrder.Items)
            {
                curItem.Product = _context.Products.Find(curItem.Product.Id);
            }

            AddEntity(newOrder);
        }
    }
}