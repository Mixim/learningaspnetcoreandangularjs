﻿using System.Collections.Generic;
using DutchTreat.Data.Entities;

namespace DutchTreat.Data.Contracts
{
    public interface IDutchRepository
    {
        IEnumerable<Product> GetAllProducts();
        IEnumerable<Product> GetProductsByCategory(string category);
        IEnumerable<Order> GetAllOrders(bool includeItems);
        IEnumerable<Order> GetAllOrdersByUser(string username, bool includeItems);
        Order GetOrderById(string username, int id);
        bool SaveAll();
        void AddEntity<T>(T entity)
            where T : class;
        void AddOrder(Order newOrder);
    }
}