﻿using System;
using System.Collections.Generic;

namespace DutchTreat.Data.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Number { get; set; }
        public ICollection<OrderItem> Items { get; set; }
        public User User { get; set; }

        public Order(int id, DateTime date, string number, ICollection<OrderItem> items, User user)
        {
            Id = id;
            Date = date;
            Number = number;
            Items = items;
            User = user;
        }

        public Order(DateTime date, string number, ICollection<OrderItem> items, User user)
            : this(default(int), date, number, items, user)
        {
        }

        public Order()
            : this(default(DateTime), default(string), new List<OrderItem>(), default(User))
        {
        }
    }
}