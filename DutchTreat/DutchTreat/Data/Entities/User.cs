﻿using Microsoft.AspNetCore.Identity;

namespace DutchTreat.Data.Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public User(string firstName, string lastName, string userName, string email)
            : base(userName)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public User()
            : this(default(string), default(string), default(string), default(string))
        {

        }
    }
}