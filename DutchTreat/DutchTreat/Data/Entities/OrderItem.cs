﻿

namespace DutchTreat.Data.Entities
{
    public class OrderItem
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public Order Order { get; set; }

        public OrderItem(int id, Product product, int quantity, decimal unitPrice, Order order)
        {
            Id = id;
            Product = product;
            Quantity = quantity;
            UnitPrice = unitPrice;
            Order = order;
        }

        public OrderItem(Product product, int quantity, decimal unitPrice, Order order)
            : this(default(int), product, quantity, unitPrice, order)
        {
        }

        public OrderItem(Product product, int quantity, decimal unitPrice)
            : this(product, quantity, unitPrice, default(Order))
        {
        }

        public OrderItem()
            : this(default(Product), default(int), default(decimal))
        {
        }
    }
}