﻿using DutchTreat.Data.Entities;
using DutchTreat.Helpers.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DutchTreat.Helpers
{
    /// <summary>
    /// Helper for authentication.
    /// </summary>
    public static class AuthenticationHelper
    {
        #region Constants.

        public const string SymmetricSecurityKeyConfigName = "Tokens:Key";
        public const string TokenIssuerConfigName = "Tokens:Issuer";
        public const string TokenAudienceConfigName = "Tokens:Audience";
        private const string TokenLifetimeConfigName = "Tokens:Lifetime";

        public const string AuthorizationHeaderName = "Authorization";
        public const string DefaultAuthenticationScheme = "Identity.Application";
        public const string AdditionalAuthenticationScheme = JwtBearerDefaults.AuthenticationScheme;

        public const string UsedAuthSchemes = DefaultAuthenticationScheme + "," + AdditionalAuthenticationScheme;

        #endregion

        public static string ForwardDefaultSelector(HttpContext httpContext)
        {
            string returnValue;
            if (httpContext.Request.Headers.TryGetValue(AuthorizationHeaderName, out StringValues authHeader))
            {
                if (authHeader.ToString().StartsWith(AdditionalAuthenticationScheme, StringComparison.InvariantCultureIgnoreCase))
                {
                    returnValue = AdditionalAuthenticationScheme;
                }
                else
                {
                    returnValue = DefaultAuthenticationScheme;
                }
            }
            else
            {
                returnValue = DefaultAuthenticationScheme;
            }
            return returnValue;
        }

        private static SecurityToken PrepareSecurityToken(User preparedUser, IConfiguration config)
        {
            SecurityToken returnValue;

            returnValue = PrepareSecurityToken(preparedUser,
                config[TokenIssuerConfigName],
                config[TokenAudienceConfigName],
                Convert.ToDouble(config[TokenLifetimeConfigName]),
                config[SymmetricSecurityKeyConfigName]);

            return returnValue;
        }

        private static SecurityToken PrepareSecurityToken(User preparedUser, string issuer, string audience, double lifetime, string key)
        {
            SecurityToken returnValue;
            var claims = new[]
                        {
                            new Claim(ClaimTypes.Name, preparedUser.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            returnValue = new JwtSecurityToken(
                issuer,
                audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(lifetime),
                signingCredentials: credentials);

            return returnValue;
        }

        public static TokenRepresentation PrepareToken(User preparedUser, IConfiguration config)
        {
            SecurityToken rawSecurityToken;
            TokenRepresentation returnValue;

            rawSecurityToken = PrepareSecurityToken(preparedUser, config);
            string token = new JwtSecurityTokenHandler().WriteToken(rawSecurityToken);
            returnValue = new TokenRepresentation(token, rawSecurityToken.ValidTo);

            return returnValue;
        }
    }
}