﻿using System;

namespace DutchTreat.Helpers.Common
{
    /// <summary>
    /// Representation of the token.
    /// </summary>
    public class TokenRepresentation
    {
        /// <summary>
        /// String representation of token.
        /// </summary>
        public string Token { get; protected set; }
        /// <summary>
        /// Expiration date.
        /// </summary>
        public DateTime Expiration { get; protected set; }

        /// <summary>
        /// Initializes a new instance of <see cref="TokenRepresentation"/>.
        /// </summary>
        /// <param name="token">String representation of token.</param>
        /// <param name="expiration">Expiration date.</param>
        public TokenRepresentation(string token, DateTime expiration)
        {
            Token = token;
            Expiration = expiration;
        }
    }
}