﻿using Microsoft.Extensions.Logging;

namespace DutchTreat.Services.MailService
{
    public class NullMailService : IMailService
    {
        protected ILogger<IMailService> Logger { get; }

        public NullMailService(ILogger<IMailService> logger)
        {
            Logger = logger;
        }

        public void SendMessage(string to, string subject, string body)
        {
            Logger.LogInformation($"To: \"{to}\"; Subject: \"{subject}\" Body: \"{body}\".");
        }
    }
}