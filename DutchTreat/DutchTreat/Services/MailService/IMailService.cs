﻿

namespace DutchTreat.Services.MailService
{
    public interface IMailService
    {
        void SendMessage(string to, string subject, string body);
    }
}