﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace DutchTreat
{
    public class Program
    {
        #region Constants.

        protected const string NameOfJsonConfigFile = "config.json";

        protected const string NameOfXmlConfigFile = "config.xml";

        #endregion

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration(SetupConfiguration)
                .UseStartup<Startup>()
                .Build();

        private static void SetupConfiguration(WebHostBuilderContext context, IConfigurationBuilder builder)
        {
            builder.Sources.Clear();

            builder.AddJsonFile(NameOfJsonConfigFile, false, true)
                   .AddXmlFile(NameOfXmlConfigFile, true)
                   .AddEnvironmentVariables();
        }
    }
}