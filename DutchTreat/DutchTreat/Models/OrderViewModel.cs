﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DutchTreat.Models
{
    public class OrderViewModel
    {
        public int CoolId { get; set; }
        public DateTime Date { get; set; }
        [Required]
        [MinLength(4)]
        public string Number { get; set; }

        public ICollection<OrderItemViewModel> Items { get; set; }
    }
}