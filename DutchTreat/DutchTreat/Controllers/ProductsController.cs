﻿using DutchTreat.Data.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace DutchTreat.Controllers
{
    [Route("api/[Controller]")]
    public class ProductsController : Controller
    {
        public IDutchRepository _repository { get; }
        public ILogger<ProductsController> _logger { get; }

        public ProductsController(IDutchRepository repository, ILogger<ProductsController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            IActionResult returnValue;
            try
            {
                returnValue = Ok(_repository.GetAllProducts());
            }
            catch(Exception ex)
            {
                _logger.LogError($"Failed to get products: \"{ex}\".");
                returnValue = BadRequest("Failed to get products.");
            }
            return returnValue;
        }
    }
}