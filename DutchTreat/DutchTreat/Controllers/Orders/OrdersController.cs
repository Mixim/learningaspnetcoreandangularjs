﻿using AutoMapper;
using DutchTreat.Data.Contracts;
using DutchTreat.Data.Entities;
using DutchTreat.Helpers;
using DutchTreat.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DutchTreat.Controllers.Orders
{
    [Route("api/[Controller]")]
    [Authorize(AuthenticationSchemes = AuthenticationHelper.UsedAuthSchemes)]
    public class OrdersController : Controller
    {
        public IDutchRepository _repository { get; }
        public ILogger<OrdersController> _logger { get; }
        public IMapper _mapper { get; }
        public UserManager<User> _userManager { get; }

        public OrdersController(IDutchRepository repository,
            ILogger<OrdersController> logger,
            IMapper mapper,
            UserManager<User> userManager)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Get(bool includeItems = true)
        {
            IActionResult returnValue;
            try
            {
                var username = User.Identity.Name;

                IEnumerable<Order> preparedOrders = _repository.GetAllOrdersByUser(username, includeItems);

                returnValue = Ok(_mapper.Map<IEnumerable<Order>, IEnumerable<OrderViewModel>>(preparedOrders));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get orders: \"{ex}\".");
                returnValue = BadRequest("Failed to get orders.");
            }
            return returnValue;
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            IActionResult returnValue;
            try
            {
                Order order = _repository.GetOrderById(User.Identity.Name, id);

                if (order != null)
                {
                    returnValue = Ok(_mapper.Map<Order, OrderViewModel>(order));
                }
                else
                {
                    returnValue = NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get orders: \"{ex}\".");
                returnValue = BadRequest("Failed to get orders.");
            }
            return returnValue;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] OrderViewModel order)
        {
            IActionResult returnValue;
            try
            {
                if (ModelState.IsValid)
                {
                    var newOrder = _mapper.Map<OrderViewModel, Order>(order);
                    if (newOrder.Date == default(DateTime))
                    {
                        newOrder.Date = DateTime.Now;
                    }

                    var currentUser = await _userManager.FindByNameAsync(User.Identity.Name);
                    newOrder.User = currentUser;

                    _repository.AddOrder(newOrder);
                    if (_repository.SaveAll())
                    {
                        returnValue = Created($"/api/orders/{newOrder.Id}", newOrder);
                    }
                    else
                    {
                        returnValue = BadRequest("Failed to save order.");
                    }
                }
                else
                {
                    returnValue = BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to save order: \"{ex}\".");
                returnValue = BadRequest("Failed to save order.");
            }
            return returnValue;
        }
    }
}