﻿using AutoMapper;
using DutchTreat.Data.Contracts;
using DutchTreat.Data.Entities;
using DutchTreat.Helpers;
using DutchTreat.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DutchTreat.Controllers.Orders
{
    [Route("api/orders/{orderId:int}/items")]
    [Authorize(AuthenticationSchemes = AuthenticationHelper.UsedAuthSchemes)]
    public class OrderItemsController : Controller
    {
        public IDutchRepository _repository { get; }
        public ILogger<OrdersController> _logger { get; }
        public IMapper _mapper { get; }

        public OrderItemsController(IDutchRepository repository,
            ILogger<OrdersController> logger,
            IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get(int orderId)
        {
            IActionResult returnValue;
            try
            {
                Order order = _repository.GetOrderById(User.Identity.Name, orderId);

                if (order != null)
                {
                    returnValue = Ok(_mapper.Map<IEnumerable<OrderItem>, IEnumerable<OrderItemViewModel>>(order.Items));
                }
                else
                {
                    returnValue = NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get order items: \"{ex}\".");
                returnValue = BadRequest("Failed to get order items.");
            }
            return returnValue;
        }

        [HttpGet("{requiredOrderItemId:int}")]
        public IActionResult Get(int orderId, int requiredOrderItemId)
        {
            IActionResult returnValue;
            try
            {
                Order order = _repository.GetOrderById(User.Identity.Name, orderId);

                if (order != null)
                {
                    OrderItem requiredOrderItem = order.Items.SingleOrDefault(item => item.Id == requiredOrderItemId);
                    if (requiredOrderItem != null)
                    {
                        returnValue = Ok(_mapper.Map<OrderItem, OrderItemViewModel>(requiredOrderItem));
                    }
                    else
                    {
                        returnValue = NotFound();
                    }
                }
                else
                {
                    returnValue = NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get order item: \"{ex}\".");
                returnValue = BadRequest("Failed to get order item.");
            }
            return returnValue;
        }
    }
}