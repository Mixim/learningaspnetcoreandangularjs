﻿using DutchTreat.Data.Entities;
using DutchTreat.Helpers;
using DutchTreat.Helpers.Common;
using DutchTreat.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace DutchTreat.Controllers
{
    public class AccountController : Controller
    {
        public ILogger<ProductsController> _logger { get; }
        public SignInManager<User> _signInManager { get; }
        public UserManager<User> _userManager { get; }
        public IConfiguration _config { get; }

        public AccountController(ILogger<ProductsController> logger,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            IConfiguration config)
        {
            _logger = logger;
            _signInManager = signInManager;
            _userManager = userManager;
            _config = config;
        }

        public IActionResult Login()
        {
            IActionResult returnValue;

            if (User.Identity.IsAuthenticated)
            {
                returnValue = RedirectToAction(nameof(AppController.Index), "App");
            }
            else
            {
                returnValue = View();
            }

            return returnValue;
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel user)
        {
            IActionResult returnValue;
            if (ModelState.IsValid)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(user.Username, user.Password, user.RememberMe, false);
                if (signInResult.Succeeded)
                {
                    if (Request.Query.Keys.Contains("ReturnUrl"))
                    {
                        returnValue = Redirect(Request.Query["ReturnUrl"].First());
                    }
                    else
                    {
                        returnValue = RedirectToAction(nameof(AppController.Index), "App");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Failed to login");
                    returnValue = View();
                }
            }
            else
            {
                ModelState.AddModelError("", "Failed to login");
                returnValue = View();
            }
            return returnValue;
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(AppController.Index), "App");
        }

        [HttpPost]
        public async Task<IActionResult> CreateToken([FromBody] LoginViewModel user)
        {
            IActionResult returnValue;
            if(ModelState.IsValid)
            {
                var preparedUser = await _userManager.FindByNameAsync(user.Username);
                if (preparedUser != null)
                {
                    var signInResult = await _signInManager.CheckPasswordSignInAsync(preparedUser, user.Password, false);
                    if (signInResult.Succeeded)
                    {
                        TokenRepresentation tokenRepresentation = AuthenticationHelper.PrepareToken(preparedUser, _config);
                        returnValue = Created("", tokenRepresentation);
                    }
                    else
                    {
                        returnValue = BadRequest();
                    }
                }
                else
                {
                    returnValue = BadRequest();
                }
            }
            else
            {
                returnValue = BadRequest();
            }
            return returnValue;
        }
    }
}