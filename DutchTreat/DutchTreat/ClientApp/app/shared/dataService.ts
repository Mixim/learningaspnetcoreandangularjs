﻿import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Product } from "./product";
import * as OrderNS from "./order";

@Injectable()
export class DataService {

    constructor(protected httpClient: HttpClient) { }

    private token: string = "";
    private tokenExpiration: Date;

    public order: OrderNS.Order = new OrderNS.Order();

    public products: Product[] = [];

    loadProducts(): Observable<boolean> {
        return this.httpClient.get("/api/products")
            .pipe(map((data: any[]) => {
                this.products = data;
                return true;
            }));
    }

    public get loginRequired(): boolean {
        return this.token.length == 0 || this.tokenExpiration > new Date();
    }

    login(credentials): Observable<boolean> {
        return this.httpClient
            .post("/account/createToken", credentials)
            .pipe(map((data: any) => {
                this.token = data.token;
                this.tokenExpiration = data.expiration;
                return true;
            }));
    }

    public checkout() {
        if (!this.order.number) {
            this.order.number = this.order.date.getFullYear().toString() + this.order.date.getTime().toString();
        }
        return this.httpClient.post("/api/orders", this.order, {
            headers: new HttpHeaders().set("Authorization", "Bearer " + this.token)
        })
            .pipe(map(response => {
                this.order = new OrderNS.Order();
                return true;
            }));
    }

    public addToOrder(newProduct: Product) {

        let item: OrderNS.OrderItem;
        item = this.order.items.find(curItem => curItem.productId == newProduct.id);

        if (item != null) {
            item.quantity += 1;
        }
        else {
            item = new OrderNS.OrderItem();
            item.productId = newProduct.id;
            item.productArtist = newProduct.artist;
            item.productArtId = newProduct.artId;
            item.productCategory = newProduct.category;
            item.productSize = newProduct.size;
            item.productTitle = newProduct.title;
            item.unitPrice = newProduct.price;
            item.quantity = 1

            this.order.items.push(item);
        }
    }

}