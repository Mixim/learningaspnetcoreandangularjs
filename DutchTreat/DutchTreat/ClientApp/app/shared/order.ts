﻿import * as lodashLib from "lodash";

export class Order {
    coolId: number;
    date: Date = new Date();
    number: string;
    items: Array<OrderItem> = new Array<OrderItem>();

    get subtotal(): number {
        return lodashLib.sum(lodashLib.map(this.items, curItem => curItem.unitPrice * curItem.quantity));
    };
}

export class OrderItem {
    id: number;
    quantity: number;
    unitPrice: number;
    productId: number;
    productCategory: string;
    productSize: string;
    productTitle: string;
    productArtist: string;
    productArtId: string;
}