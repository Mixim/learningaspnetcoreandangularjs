﻿class CustomerStore {
    public visits: number = 0;
    protected ourName: string;

    constructor(protected firstName: string, protected lastName: string) {
    }

    public showName() {
        alert(this.firstName + " " + this.lastName);
    }

    set name(val) {
        this.ourName = val;
    }

    get name() {
        return this.ourName;
    }
}