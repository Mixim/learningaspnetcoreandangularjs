﻿$(document).ready(function(){
    var x = 0;
    var s = "";

    console.log("Hello Pluralsight");

    var button = $("[id=buyButton]");
    button.on("click", function () {
        console.log("Buying item");
    })

    var productInfo = $(".product-props li");
    productInfo.on("click", function () {
        console.log("You clicked on " + $(this).text());
    });

    var $loginToggle = $("#loginToggle");
    var $popupForm = $(".popup-form");

    $loginToggle.on("click", function () {
        $popupForm.fadeToggle(1000);
    })
});